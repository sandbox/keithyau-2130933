<?php

/**
 * @file
 * Administation UI for AWS SNS.
 */

/**
 * Settings page.
 */
function awssns_admin($form, &$form_state) {
  $form = array();

  $default = variable_get('awssns', _awssns_default());

  $form['awssns'] = array(
    '#tree' => TRUE,
  );

  $form['awssns']['aws'] = array(
    '#type' => 'fieldset',
    '#title' => t('AWS SNS Parameters'),
    '#description' => t('AWS SNS configurations'),
    '#collapsible' => FALSE,
  );

  $form['awssns']['aws']['application_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Application Name'),
    '#default_value' => $default['aws']['application_name'],
    '#required' => TRUE,
  );

  $form['awssns']['aws']['platform_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Platform type'),
    '#default_value' => $default['aws']['platform_type'],
    '#description' => t('APNS/APNS_SANDBOX/GCM'),
    '#required' => TRUE,
  );

  $form['awssns']['aws']['p12_password'] = array(
    '#type' => 'password',
    '#title' => t('P12 password'),
    '#description' => t('passphase of your certificate, null if you dont have'),
  );

  $form['awssns']['aws']['APNS_cert'] = array(
    '#type' => 'managed_file',
    '#title' => t('APNS certificate with .p12 format'),
    '#description' => t('Make it as a private file'),
    '#upload_location' => 'private://awssns_keys',
    '#default_value' => variable_get('awssns_apns_p12', 'no_file'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('pem p12'),
      // Pass the maximum file size in bytes
      'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),
    ),
  );

  //to-do a dependent dropdown for APNS & GCM
  $form['awssns']['aws']['GCM'] = array(
    '#type' => 'textfield',
    '#title' => t('Google API console server key for GCM'),
  );

  $form['awssns']['aws']['PlatformApplicationArn'] = array(
    '#type' => 'textfield',
    '#title' => t('The return platform arn from AWS if createPlatformApplication success'),
    '#default_value' => variable_get('awssns_platform_arn', ''),
  );

  $form['awssns']['aws']['topicArn'] = array(
    '#type' => 'textfield',
    '#title' => t('Register a default topic when the platformArn is created'),
    '#description' => t('Give a default topic Arn, currently you need your own way to put endpoint device token to the topic'),
    '#default_value' => variable_get('awssns_default_topicarn', ''),
  );

  // Handling file upload
  $form['#submit'][] = 'settings_page_file_save_form_submit';
  // validation
  $form['#validate'][] = 'awssns_admin_validate';

  return system_settings_form($form);
}

/**
 * Submit handler for file upload.
 */
function settings_page_file_save_form_submit($form, $form_state) {
  $file = file_load($form_state['values']['awssns']['aws']['APNS_cert']);

  if ($file) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    variable_set('awssns_apns_p12', $file->fid);

    // @TODO add file_usage_add by figuring out a unique id.
    // file_usage_add($file, 'awssns', 'awssns', $user->uid);
    unset($form_state['values']['awssns']['aws']['APNS_cert']);

    $SNSAppName = $form_state['values']['awssns']['aws']['application_name'];
    $platform_type = $form_state['values']['awssns']['aws']['platform_type'];
    // Generate .pem from .p12
    $file->filepath = drupal_realpath($file->uri);
    $password = $form_state['values']['awssns']['aws']['p12_password'];
    $ok = openssl_pkcs12_read(file_get_contents($file->filepath), $data, $password);

    if ($ok) {
      $certificate = $data['cert'];
      $privatekey = $data['pkey'];
      // Create AWSSNS platform application with the provided creditentials.
      // @TODO Support GCM as well.
      $result = MobingiAPNS::createPlatformApplication($SNSAppName,$certificate,$privatekey,$platform_type);
      variable_set('awssns_platform_arn', $result['PlatformApplicationArn']);
    } else {
      drupal_set_message('APNS Certificate is invalid. Double check your password is correct.', 'warning');
    }

    // Get a topicarn.
    if ($result) {
      $result = MobingiAPNS::createTopic('drupal_aws_rules_default');
      variable_set('awssns_default_topicarn', $result['TopicArn']);
    }
  } else {
    variable_set('awssns_apns_p12', $file->fid);
    unset($form_state['values']['awssns']['aws']['APNS_cert']);
  }
}

/**
 * Settings form validation.
 */
function awssns_admin_validate(&$form, &$form_state) {
  $values = $form_state['values'];

  return TRUE;
}

/**
 * Default settings value.
 *
 * @return An array of default value.
 */
function _awssns_default() {
  return array(
    'aws' => array(
      'application_name' => 'awstest',
      'platform_type' => 'Mobile push service type',
      'APNS_cert' => '',
      'GCM' => '',
    ),
  );
}
