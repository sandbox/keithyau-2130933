<?php

/**
 * @file
 * AWS SNS interface.
 */

interface AWSSNSAPNSInterface{
  static public function createPlatformApplication($SNSAppName,$certificate,$privatekey,$platform_type);
  static public function createPlatformEndpoint($platformApplicationArn,$token);
  static public function createTopic($name);
  static public function subscribe($topicarn,$endpoint);
  static public function publish($topicArn = NULL, $targetARN = NULL,$message);
}
