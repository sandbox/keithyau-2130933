<?php

/**
 * @file
 * AWSSNSAPNS class.
 */

require_once 'awssns.interface.inc';

class AWSSNSAPNS implements AWSSNSAPNSInterface{
  /**
   * register an app on AWS SNS.
   */
  static public function createPlatformApplication($SNSAppName, $certificate, $privatekey, $platform_type){
    libraries_load('awssdk');
    $region = 'SINGAPORE'; //if default region in awssdk ui is not set, use this setting
    $sns = awssdk_get_client('sns', $region); 
    $result = $sns->createPlatformApplication(array(
      'Name' => $SNSAppName,
      'Platform' => $platform_type,
      'Attributes' => array(
          'PlatformCredential' => $privatekey,
          'PlatformPrincipal' => $certificate,
      ),
    ));
    $result_arr = ($result->toArray());
    if($result_arr["PlatformApplicationArn"]){
      return array(
        "PlatformApplicationArn" => $result_arr["PlatformApplicationArn"],
        );
    }
    else return FALSE;
  }

  static public function createPlatformEndpoint($platformApplicationArn, $token){
    libraries_load('awssdk');
    $region = 'SINGAPORE';
    $sns = awssdk_get_client('sns', $region);
    $result = $sns->createPlatformEndpoint(array(
      'PlatformApplicationArn' => $platformApplicationArn,
      'Token' => $token,
    ));
    $result_arr = ($result->toArray());
    if($result_arr["EndpointArn"]){
      return array(
        "EndpointArn" => $result_arr["EndpointArn"],
        );
    }
    else return FALSE;
  }

  static public function createTopic($name){
    libraries_load('awssdk');
    $region = 'SINGAPORE';
    $sns = awssdk_get_client('sns', $region);
    $result = $sns->createTopic(array(
      'Name' => $name,
    ));
    $result_arr = ($result->toArray());
    if($result_arr["TopicArn"]){
      return array(
        "TopicArn" => $result_arr["TopicArn"],
        );
    }
    else return FALSE;
  }

  static public function subscribe($topicarn,$endpoint){
    libraries_load('awssdk');
    $region = 'SINGAPORE';
    $sns = awssdk_get_client('sns', $region);
    $result = $sns->subscribe(array(
      'TopicArn' => $topicarn,
      'Protocol' => 'application',
      'Endpoint' => $endpoint,
    ));
    $result_arr = ($result->toArray());
    if($result_arr["SubscriptionArn"]){
      return array(
        "SubscriptionArn" => $result_arr["SubscriptionArn"],
        );
    }
    else return FALSE;
  }

  static public function publish($topicArn = NULL, $targetARN = NULL,$message){
    libraries_load('awssdk');
    $region = 'SINGAPORE';
    $sns = awssdk_get_client('sns', $region);
    $result = $sns->publish(array(
      'TopicArn' => $topicArn,
      'TargetArn' => $targetARN,
      'Message' => $message,
    ));
    $result_arr = ($result->toArray());
    if($result_arr["MessageId"]){
      return array(
        "MessageId" => $result_arr["MessageId"],
        "Message" => $message,
        "TopicArn" => $topicArn,
        "TargetArn" => $targetARN,
        );
    }
    else return FALSE;
  }
}
